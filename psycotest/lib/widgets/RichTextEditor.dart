import 'package:flutter/material.dart';
import 'package:zefyr/zefyr.dart';
import 'package:psycotest/resources/strings.dart';

class RichTextEditor extends StatefulWidget {
  final String fieldName;
  final EditEndsCallback onEditingEnds;

  RichTextEditor({this.onEditingEnds, this.fieldName});

  @override
  State<StatefulWidget> createState() => _RichTextEditorState();
}

class _RichTextEditorState extends State<RichTextEditor> {
  ZefyrController _controller;
  FocusNode _focusNode;
  @override
  void initState() {
    super.initState();
    // Create an empty document or load existing if you have one.
    // Here we create an empty document:
    final document = new NotusDocument();
    _controller = new ZefyrController(document);
    _focusNode = new FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () => Navigator.of(context).pop(null),
            );
          },
        ),
        title: Text('${Strings.values['es']['editing']} $widget.fieldName'),
      ),
      body: Center(
        child: ZefyrScaffold(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 20, 0, 60),
            child: ZefyrEditor(
              controller: _controller,
              focusNode: _focusNode,
            ),
          ),
        ),
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(bottom: 60),
        child: FloatingActionButton(
          onPressed: () => widget.onEditingEnds(_controller.document.toJson()),
          tooltip: Strings.values['es']['endEditingToolTip'],
          child: Icon(Icons.check),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}

typedef EditEndsCallback = void Function(String document);

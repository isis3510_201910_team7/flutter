import 'package:flutter/material.dart';
import 'package:zefyr/zefyr.dart';

class RichTextViewer extends StatefulWidget {
  final String document;
  final String documentName;

  RichTextViewer({this.document, this.documentName});

  @override
  State<StatefulWidget> createState() => _RichTextViewerState();
}

class _RichTextViewerState extends State<RichTextViewer> {
  ZefyrController _controller;
  FocusNode _focusNode;
  @override
  void initState() {
    super.initState();
    // Create an empty document or load existing if you have one.
    // Here we create an empty document:
    _controller = new ZefyrController(NotusDocument.fromJson(widget.document));
    _focusNode = new FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () => Navigator.of(context).pop(null),
            );
          },
        ),
        title: Text('$widget.documentName'),
      ),
      body: Center(
        child: ZefyrScaffold(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
            child: ZefyrEditor(
              enabled: false,
              controller: _controller,
              focusNode: _focusNode,
            ),
          ),
        ),
      ),
    );
  }
}

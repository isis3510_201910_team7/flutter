import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import './index.dart';

class CreateTestScreen extends StatefulWidget {
  const CreateTestScreen({
    Key key,
    @required CreateTestBloc createTestBloc,
  })  : _createTestBloc = createTestBloc,
        super(key: key);

  final CreateTestBloc _createTestBloc;

  @override
  CreateTestScreenState createState() {
    return new CreateTestScreenState(_createTestBloc);
  }
}

class CreateTestScreenState extends State<CreateTestScreen> {
  final CreateTestBloc _createTestBloc;
  CreateTestScreenState(this._createTestBloc);

  @override
  void initState() {
    super.initState();
    this._createTestBloc.dispatch(LoadCreateTestEvent());
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CreateTestEvent, CreateTestState>(
        bloc: widget._createTestBloc,
        builder: (
          BuildContext context,
          CreateTestState currentState,
        ) {
          if (currentState is UnCreateTestState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (currentState is ErrorCreateTestState) {
            return new Container(
              child: new Center(
              child: new Text(currentState.errorMessage ?? 'Error' ),
            ));
          }
          return new Container(
              child: new Center(
            child: new Text("В разработке"),
          ));
        });
  }
}

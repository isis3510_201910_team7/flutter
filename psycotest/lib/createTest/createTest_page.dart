import 'package:flutter/material.dart';
import './index.dart';

class CreateTestPage extends StatelessWidget {
  static const String routeName = "/createTest";

  @override
  Widget build(BuildContext context) {
    var _createTestBloc = new CreateTestBloc();
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("CreateTest"),
      ),
      body: new CreateTestScreen(createTestBloc: _createTestBloc),
    );
  }
}

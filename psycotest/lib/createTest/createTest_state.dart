import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CreateTestState extends Equatable {
  CreateTestState([Iterable props]) : super(props);

  /// Copy object for use in action
  CreateTestState getStateCopy();
}

/// UnInitialized
class UnCreateTestState extends CreateTestState {
  @override
  String toString() => 'UnCreateTestState';

  @override
  CreateTestState getStateCopy() {
    return UnCreateTestState();
  }
}

/// Initialized
class InCreateTestState extends CreateTestState {
  @override
  String toString() => 'InCreateTestState';

  @override
  CreateTestState getStateCopy() {
    return InCreateTestState();
  }
}

class ErrorCreateTestState extends CreateTestState {
  final String errorMessage;

  ErrorCreateTestState(this.errorMessage);
  
  @override
  String toString() => 'ErrorCreateTestState';

  @override
  CreateTestState getStateCopy() {
    return ErrorCreateTestState(this.errorMessage);
  }
}

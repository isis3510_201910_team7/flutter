import 'dart:async';
import './index.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CreateTestEvent {
  Future<CreateTestState> applyAsync(
      {CreateTestState currentState, CreateTestBloc bloc});
  final CreateTestProvider _createTestProvider = new CreateTestProvider();
}

class LoadCreateTestEvent extends CreateTestEvent {
  @override
  String toString() => 'LoadCreateTestEvent';

  @override
  Future<CreateTestState> applyAsync(
      {CreateTestState currentState, CreateTestBloc bloc}) async {
    try {
      await Future.delayed(new Duration(seconds: 2));
      this._createTestProvider.test();
      return new InCreateTestState();
    } catch (_) {
      print('LoadCreateTestEvent ' + _?.toString());
      return new ErrorCreateTestState(_?.toString());
    }
  }
}

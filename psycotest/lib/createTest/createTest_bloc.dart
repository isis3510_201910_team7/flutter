import 'dart:async';
import 'package:bloc/bloc.dart';
import './index.dart';

class CreateTestBloc extends Bloc<CreateTestEvent, CreateTestState> {
  static final CreateTestBloc _createTestBlocSingleton = new CreateTestBloc._internal();
  factory CreateTestBloc() {
    return _createTestBlocSingleton;
  }
  CreateTestBloc._internal();
  
  CreateTestState get initialState => new UnCreateTestState();

  @override
  Stream<CreateTestState> mapEventToState(
    CreateTestEvent event,
  ) async* {
    try {
      yield await event.applyAsync(currentState: currentState, bloc: this);
    } catch (_) {
      print('CreateTestBloc ' + _?.toString());
      yield currentState;
    }
  }
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'TestStructure.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TestStructure _$TestStructureFromJson(Map<String, dynamic> json) {
  return TestStructure(json['name'] as String, json['description'] as String,
      json['instructions'] as String)
    ..elements = json['elements'] as List;
}

Map<String, dynamic> _$TestStructureToJson(TestStructure instance) =>
    <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'instructions': instance.instructions,
      'elements': instance.elements
    };

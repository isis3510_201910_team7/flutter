import 'dart:io';
import 'dart:convert';
import 'dart:typed_data';
import 'package:json_annotation/json_annotation.dart';
import 'package:psycotest/models/TestEditor/SuperClasses/TestElement.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'ImageStatement.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class ImageStatement extends Element {
  String image;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory ImageStatement.fromJson(Map<String, dynamic> json) =>
      _$ImageStatementFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$ImageStatementToJson(this);

  ImageStatement();

  void addImageFromFile(File imageFile) {
    List<int> imageBytes = imageFile.readAsBytesSync();
    image = base64Encode(imageBytes);
  }

  File getEncodedImage() {
    Uint8List imageBytes = base64Decode(image);
    return File.fromRawPath(imageBytes);
  }

  @override
  ElementType getType() {
    return ElementType.imageStatement;
  }
}

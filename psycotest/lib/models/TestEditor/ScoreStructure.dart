import 'package:psycotest/models/TestEditor/TextStatement.dart';
import 'package:json_annotation/json_annotation.dart';

/// This allows the `NumericalScore` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'ScoreStructure.g.dart';

enum SCORE_DISPLAY { SHOW_ALL, DROPDOWN, LIKERT }

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class NumericalScore {
  int initial;
  int jump;
  int end;
  SCORE_DISPLAY display;

  factory NumericalScore.fromJson(Map<String, dynamic> json) =>
      _$NumericalScoreFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$NumericalScoreToJson(this);

  NumericalScore();

  List<int> getScoreOptions() {
    List<int> ans = new List();
    for (var i = initial; i < end; i += jump) {
      ans.add(i);
    }
    return ans;
  }

  static NumericalScore fromMapDirect(Map b) {}
}

@JsonSerializable()
class TextualScore {
  String id;
  String type;
  String typeScore;
  bool visibleExaminee;
  List<TextStatement> options;
  List<String> simpleOptions;
  SCORE_DISPLAY display;

  factory TextualScore.fromJson(Map<String, dynamic> json) =>
      _$TextualScoreFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$TextualScoreToJson(this);

  TextualScore();

  void addOption(String option) {
    options.add(new TextStatement.complete(option));
  }

  void deleteOptionAt(int index) {
    options.removeAt(index);
  }

  TextualScore.fromMapDirect(Map map) {
    id = map['id'];
    type = map['tipo'];
    typeScore = map['tipoScore'];
    visibleExaminee = map['visibleExaminado'];
    simpleOptions = new List();
    Map a = Map<String, String>.from(map['configuracion']) ;
    try {
      (a as Map<String,String>).forEach((id, option ) {
        simpleOptions.add(option);
      });
    } catch (e) {
      print("TExtual Score structure: error -" + e.toString());
    }
  }
}

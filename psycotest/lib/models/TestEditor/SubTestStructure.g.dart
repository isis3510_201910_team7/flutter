// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'SubTestStructure.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SubTestStructure _$SubTestStructureFromJson(Map<String, dynamic> json) {
  return SubTestStructure(json['name'] as String, json['description'] as String,
      json['instructions'] as String)
    ..id = json['id'] as String
    ..elements = json['elements'] as List;
}

Map<String, dynamic> _$SubTestStructureToJson(SubTestStructure instance) =>
    <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'instructions': instance.instructions,
      'id': instance.id,
      'elements': instance.elements
    };

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'QualificationTool.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

QualificationTool _$QualificationToolFromJson(Map<String, dynamic> json) {
  return QualificationTool()
    ..id = json['id'] as String
    ..type = _$enumDecodeNullable(_$TOOL_TYPEEnumMap, json['type'])
    ..typeString = json['typeString'] as String
    ..visibleExaminee = json['visibleExaminee'] as bool;
}

Map<String, dynamic> _$QualificationToolToJson(QualificationTool instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': _$TOOL_TYPEEnumMap[instance.type],
      'typeString': instance.typeString,
      'visibleExaminee': instance.visibleExaminee
    };

T _$enumDecode<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }
  return enumValues.entries
      .singleWhere((e) => e.value == source,
          orElse: () => throw ArgumentError(
              '`$source` is not one of the supported values: '
              '${enumValues.values.join(', ')}'))
      .key;
}

T _$enumDecodeNullable<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source);
}

const _$TOOL_TYPEEnumMap = <TOOL_TYPE, dynamic>{
  TOOL_TYPE.VOICE_RECORD: 'VOICE_RECORD',
  TOOL_TYPE.PHOTO_SHOOT: 'PHOTO_SHOOT',
  TOOL_TYPE.TIME_RECORD: 'TIME_RECORD',
  TOOL_TYPE.TEXT_AREA: 'TEXT_AREA'
};

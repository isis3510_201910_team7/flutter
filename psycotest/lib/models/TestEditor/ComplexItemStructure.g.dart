// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ComplexItemStructure.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ComplexItemStructure _$ComplexItemStructureFromJson(Map<String, dynamic> json) {
  return ComplexItemStructure()
    ..trials = (json['trials'] as List)
        ?.map((e) => e == null
            ? null
            : TrialStructure.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$ComplexItemStructureToJson(
        ComplexItemStructure instance) =>
    <String, dynamic>{'trials': instance.trials};

import 'package:json_annotation/json_annotation.dart';
import 'package:psycotest/models/TestEditor/SuperClasses/TestElement.dart';
import 'package:psycotest/models/TestEditor/TrialStructure.dart';

/// This allows the `ComplexItem` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'ComplexItemStructure.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class ComplexItemStructure extends Element{
  List<TrialStructure> trials;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory ComplexItemStructure.fromJson(Map<String, dynamic> json) =>
      _$ComplexItemStructureFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$ComplexItemStructureToJson(this);

  ComplexItemStructure() {
    trials = new List();
  }

  @override
  ElementType getType() {
    return ElementType.complexItem;
  }
}

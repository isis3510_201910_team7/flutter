// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'SimpleItemStructure.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SimpleItemStructure _$SimpleItemStructureFromJson(Map<String, dynamic> json) {
  return SimpleItemStructure()
    ..id = json['id'] as String
    ..name = json['name'] as String
    ..type = json['type'] as String
    ..visibleExaminee = json['visibleExaminee'] as bool
    ..elements = json['elements'] as List;
}

Map<String, dynamic> _$SimpleItemStructureToJson(
        SimpleItemStructure instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'type': instance.type,
      'visibleExaminee': instance.visibleExaminee,
      'elements': instance.elements
    };

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ImageStatement.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ImageStatement _$ImageStatementFromJson(Map<String, dynamic> json) {
  return ImageStatement()..image = json['image'] as String;
}

Map<String, dynamic> _$ImageStatementToJson(ImageStatement instance) =>
    <String, dynamic>{'image': instance.image};

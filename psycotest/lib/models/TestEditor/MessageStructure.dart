import 'package:json_annotation/json_annotation.dart';
import 'package:psycotest/models/TestEditor/SuperClasses/TestElement.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'MessageStructure.g.dart';

enum MESSAGE_TYPE { ALERT, WARNING, INFORMATION }

@JsonSerializable()
class MessageStructure extends Element{
  MESSAGE_TYPE type;
  String text;
  String id;
  String name;
  bool visibleExaminee;
  String typeString;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory MessageStructure.fromJson(Map<String, dynamic> json) =>
      _$MessageStructureFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$MessageStructureToJson(this);

  MessageStructure(this.type, this.text);

  @override
  ElementType getType() {
    return ElementType.message;
  }

  MessageStructure.fromMapDirect(Map map) {
    id = map['id'];
    name = map['nombre'];
    text = map['mensaje'];
    typeString = map['tipo'];
    visibleExaminee = map['visibleExaminado'];
  }
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'TestEditor.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TestEditor _$TestEditorFromJson(Map<String, dynamic> json) {
  return TestEditor()
    ..tests = (json['tests'] as List)
        ?.map((e) => e == null
            ? null
            : TestStructure.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..currentTestIndex = json['currentTestIndex'] as int;
}

Map<String, dynamic> _$TestEditorToJson(TestEditor instance) =>
    <String, dynamic>{
      'tests': instance.tests,
      'currentTestIndex': instance.currentTestIndex
    };

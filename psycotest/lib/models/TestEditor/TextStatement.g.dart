// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'TextStatement.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TextStatement _$TextStatementFromJson(Map<String, dynamic> json) {
  return TextStatement()
    ..id = json['id'] as String
    ..text = json['text'] as String
    ..type = json['type'] as String
    ..visibleExaminee = json['visibleExaminee'] as bool;
}

Map<String, dynamic> _$TextStatementToJson(TextStatement instance) =>
    <String, dynamic>{
      'id': instance.id,
      'text': instance.text,
      'type': instance.type,
      'visibleExaminee': instance.visibleExaminee
    };

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MessageStructure.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MessageStructure _$MessageStructureFromJson(Map<String, dynamic> json) {
  return MessageStructure(
      _$enumDecodeNullable(_$MESSAGE_TYPEEnumMap, json['type']),
      json['text'] as String)
    ..id = json['id'] as String
    ..name = json['name'] as String
    ..visibleExaminee = json['visibleExaminee'] as bool
    ..typeString = json['typeString'] as String;
}

Map<String, dynamic> _$MessageStructureToJson(MessageStructure instance) =>
    <String, dynamic>{
      'type': _$MESSAGE_TYPEEnumMap[instance.type],
      'text': instance.text,
      'id': instance.id,
      'name': instance.name,
      'visibleExaminee': instance.visibleExaminee,
      'typeString': instance.typeString
    };

T _$enumDecode<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }
  return enumValues.entries
      .singleWhere((e) => e.value == source,
          orElse: () => throw ArgumentError(
              '`$source` is not one of the supported values: '
              '${enumValues.values.join(', ')}'))
      .key;
}

T _$enumDecodeNullable<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source);
}

const _$MESSAGE_TYPEEnumMap = <MESSAGE_TYPE, dynamic>{
  MESSAGE_TYPE.ALERT: 'ALERT',
  MESSAGE_TYPE.WARNING: 'WARNING',
  MESSAGE_TYPE.INFORMATION: 'INFORMATION'
};

import 'package:json_annotation/json_annotation.dart';
import 'package:psycotest/models/TestEditor/ComplexItemStructure.dart';
import 'package:psycotest/models/TestEditor/MessageStructure.dart';
import 'package:psycotest/models/TestEditor/QualificationTool.dart';
import 'package:psycotest/models/TestEditor/SimpleItemStructure.dart';
import 'package:psycotest/models/TestEditor/SuperClasses/TestElement.dart';
import 'package:psycotest/models/TestEditor/TextStatement.dart';
import 'package:psycotest/resources/strings.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'SubTestStructure.g.dart';

@JsonSerializable()
class SubTestStructure extends Element {
  String name;
  String description;
  String instructions;
  String id;
  List elements;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory SubTestStructure.fromJson(Map<String, dynamic> json) =>
      _$SubTestStructureFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$SubTestStructureToJson(this);

  SubTestStructure(this.name, this.description,
      [this.instructions = Strings.VOID_INSTRUCTIONS]) {
    elements = new List();
  }

  void addMessageElement(MESSAGE_TYPE type, String text) {
    elements.add(new MessageStructure(type, text));
  }

  void addSimpleItemElement() {
    elements.add(new SimpleItemStructure());
  }

  void addComplexItemElement() {
    elements.add(new ComplexItemStructure());
  }

  @override
  ElementType getType() {
    return ElementType.subtest;
  }

  SubTestStructure.fromMapDirect(Map map) {
    name = map['nombre'];
    description = map['descripcion'];
    instructions = map['instrucciones'];
    id = map['id'];
    elements = new List();
    if (map['Bloques'] != null) {
      Map a = Map<String, dynamic>.from(map['Bloques']);
      try {
        a.forEach((id, test) {
          Map b = new Map<String, dynamic>.from(test);
          if (b['tipo'] == "ITEM") {
            SimpleItemStructure item = SimpleItemStructure.fromMapDirect(b);
            elements.add(item);
          } else if (b['tipo'] == "MENSAJE") {
            MessageStructure message = MessageStructure.fromMapDirect(b);
            elements.add(message);
          } else if (b['tipo'] == "QUALIFICATION_TOOL") {
            QualificationTool qualificationTool =
                QualificationTool.fromMapDirect(b);
            elements.add(qualificationTool);
          } else if (b['tipo'] == "TEXTO") {
            TextStatement textStatement = TextStatement.fromMapDirect(b);
            elements.add(textStatement);
          } else {
            print(b['tipo'] + ' - error');
          }
        });
      } catch (e) {
        print('Subtest: ' + e.toString());
      }
    }
  }
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'TrialStructure.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TrialStructure _$TrialStructureFromJson(Map<String, dynamic> json) {
  return TrialStructure()..elements = json['elements'] as List;
}

Map<String, dynamic> _$TrialStructureToJson(TrialStructure instance) =>
    <String, dynamic>{'elements': instance.elements};

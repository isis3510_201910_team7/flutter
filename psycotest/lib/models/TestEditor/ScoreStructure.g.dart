// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ScoreStructure.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NumericalScore _$NumericalScoreFromJson(Map<String, dynamic> json) {
  return NumericalScore()
    ..initial = json['initial'] as int
    ..jump = json['jump'] as int
    ..end = json['end'] as int
    ..display = _$enumDecodeNullable(_$SCORE_DISPLAYEnumMap, json['display']);
}

Map<String, dynamic> _$NumericalScoreToJson(NumericalScore instance) =>
    <String, dynamic>{
      'initial': instance.initial,
      'jump': instance.jump,
      'end': instance.end,
      'display': _$SCORE_DISPLAYEnumMap[instance.display]
    };

T _$enumDecode<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }
  return enumValues.entries
      .singleWhere((e) => e.value == source,
          orElse: () => throw ArgumentError(
              '`$source` is not one of the supported values: '
              '${enumValues.values.join(', ')}'))
      .key;
}

T _$enumDecodeNullable<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source);
}

const _$SCORE_DISPLAYEnumMap = <SCORE_DISPLAY, dynamic>{
  SCORE_DISPLAY.SHOW_ALL: 'SHOW_ALL',
  SCORE_DISPLAY.DROPDOWN: 'DROPDOWN',
  SCORE_DISPLAY.LIKERT: 'LIKERT'
};

TextualScore _$TextualScoreFromJson(Map<String, dynamic> json) {
  return TextualScore()
    ..id = json['id'] as String
    ..type = json['type'] as String
    ..typeScore = json['typeScore'] as String
    ..visibleExaminee = json['visibleExaminee'] as bool
    ..options = (json['options'] as List)
        ?.map((e) => e == null
            ? null
            : TextStatement.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..simpleOptions =
        (json['simpleOptions'] as List)?.map((e) => e as String)?.toList()
    ..display = _$enumDecodeNullable(_$SCORE_DISPLAYEnumMap, json['display']);
}

Map<String, dynamic> _$TextualScoreToJson(TextualScore instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'typeScore': instance.typeScore,
      'visibleExaminee': instance.visibleExaminee,
      'options': instance.options,
      'simpleOptions': instance.simpleOptions,
      'display': _$SCORE_DISPLAYEnumMap[instance.display]
    };

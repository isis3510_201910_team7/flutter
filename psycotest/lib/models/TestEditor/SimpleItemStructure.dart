import 'package:json_annotation/json_annotation.dart';
import 'package:psycotest/models/TestEditor/QualificationTool.dart';
import 'package:psycotest/models/TestEditor/ScoreStructure.dart';
import 'package:psycotest/models/TestEditor/SuperClasses/TestElement.dart';
import 'package:psycotest/models/TestEditor/TextStatement.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'SimpleItemStructure.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.
@JsonSerializable()
class SimpleItemStructure extends Element {
  String id;
  String name;
  String type;
  bool visibleExaminee;
  List elements = new List();

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory SimpleItemStructure.fromJson(Map<String, dynamic> json) =>
      _$SimpleItemStructureFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$SimpleItemStructureToJson(this);
  SimpleItemStructure() {
    elements = new List();
    visibleExaminee = false;
  }

  @override
  ElementType getType() {
    return ElementType.simpleItem;
  }

  SimpleItemStructure.fromMapDirect(Map map) {
    name = map['nombre'];
    id = map['id'];
    type = map['tipo'];
    elements = new List();
    if (map['Bloques'] != null) {
      Map a = Map<String, dynamic>.from(map['Bloques']);
      try {
        a.forEach((id, test) {
          Map b = new Map<String, dynamic>.from(test);
          if (b['tipo'] == "TEXTO") {
            TextStatement text = TextStatement.fromMapDirect(b);
            elements.add(text);
          } else if (b['tipo'] == "QUALIFICATION_TOOL") {
            QualificationTool qualificationTool =
                QualificationTool.fromMapDirect(b);
            elements.add(qualificationTool);
          } else if (b['tipo'] == "SCORE" && b['tipoScore'] == "Numérico") {
            NumericalScore score = NumericalScore.fromMapDirect(b);
            elements.add(score);
          } else if (b['tipo'] == "SCORE" && b['tipoScore'] == "Textual") {
            TextualScore score = TextualScore.fromMapDirect(b);
            elements.add(score);
          }
        });
      } catch (e) {
        print('item: ' + e.toString());
      }
    }
  }
}

import 'package:psycotest/models/TestEditor/ComplexItemStructure.dart';
import 'package:psycotest/models/TestEditor/SimpleItemStructure.dart';
import 'package:psycotest/models/TestEditor/MessageStructure.dart';
import 'package:psycotest/models/TestEditor/SubTestStructure.dart';
import 'package:psycotest/models/TestEditor/SuperClasses/TestElement.dart';
import 'package:psycotest/resources/strings.dart';
import 'package:scoped_model/scoped_model.dart';

import 'package:json_annotation/json_annotation.dart';

part 'TestStructure.g.dart';

@JsonSerializable()
class TestStructure extends Model {
  String name;
  String description;
  String instructions;
  String id;

  List<Element> elements;
  int currentElement;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory TestStructure.fromJson(Map<String, dynamic> json) =>
      _$TestStructureFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$TestStructureToJson(this);

  TestStructure(this.name, this.description,
      [this.instructions = Strings.VOID_INSTRUCTIONS]) {
    elements = new List();
    currentElement = -1;
  }

  TestStructure.fromMap(Map<String, dynamic> map, String id) {
    name = map['nombre'];
    description = map['descripcion'];
    instructions = map['instrucciones'];
    elements=[];
    this.id = id;
  }

  void addSubTestElement(String name, String description,
      [String instructions]) {
    elements.add(new SubTestStructure(name, description, instructions));
  }

  void addMessageElement(MESSAGE_TYPE type, String text) {
    elements.add(new MessageStructure(type, text));
  }

  void addSimpleItemElement() {
    elements.add(new SimpleItemStructure());
  }

  void addComplexItemElement() {
    elements.add(new ComplexItemStructure());
  }

  void editMessageElement(
      MessageStructure oldMessage, MESSAGE_TYPE type, String text) {
    for (int i = 0; i < elements.length; i++) {
      if (elements[i] == oldMessage) {
        elements[i] = new MessageStructure(type, text);
      }
    }
  }

  TestStructure.fromMapDirect(Map map) {
    name = map['nombre'];
    description = map['descripcion'];
    instructions = map['instrucciones'];
    id = map['id'];
    elements = new List();
    if(map['Bloques']!=null){    
    Map a = Map<String, dynamic>.from(map['Bloques']);
      try {
        a.forEach((id, test) {
          Map b = new Map<String, dynamic>.from(test);
          if (b['tipo'] == "SUBTEST") {
            SubTestStructure subtest = SubTestStructure.fromMapDirect(b);
            elements.add(subtest);
          } else if (b['tipo'] == "ITEM") {
            SimpleItemStructure item = SimpleItemStructure.fromMapDirect(b);
            elements.add(item);
          } else if (b['tipo'] == "MENSAJE") {
            MessageStructure message = MessageStructure.fromMapDirect(b);
            elements.add(message);
          }
        });
      } catch (e) {
        print('TestStructure: '+e.toString());
      }
    }
  }

  Map toMap() {
    Map toReturn = new Map();
    toReturn['id'] = id;
    toReturn['nombre'] = name;
    toReturn['descripcion'] = description;
    toReturn['instrucciones'] = instructions;
    return toReturn;
  }
}

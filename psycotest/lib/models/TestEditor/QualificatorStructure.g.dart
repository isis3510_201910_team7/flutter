// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'QualificatorStructure.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

QualificatorStructure _$QualificatorStructureFromJson(
    Map<String, dynamic> json) {
  return QualificatorStructure()..elements = json['elements'] as List;
}

Map<String, dynamic> _$QualificatorStructureToJson(
        QualificatorStructure instance) =>
    <String, dynamic>{'elements': instance.elements};

import 'package:json_annotation/json_annotation.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'QualificationTool.g.dart';

/// An annotation for the code generator to know that this class needs the
/// JSON serialization logic to be generated.

enum TOOL_TYPE { VOICE_RECORD, PHOTO_SHOOT, TIME_RECORD, TEXT_AREA }

@JsonSerializable()
class QualificationTool {
  String id;
  TOOL_TYPE type;
  String typeString;
  bool visibleExaminee;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory QualificationTool.fromJson(Map<String, dynamic> json) =>
      _$QualificationToolFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$QualificationToolToJson(this);

  QualificationTool();

  QualificationTool.fromMapDirect(Map map) {
    id=map['id'];
    typeString = map['tipo'];
    visibleExaminee = map['visibleExaminado'];
  }
}

enum ElementType { message, subtest, simpleItem, complexItem, trial, qualificator, qualificationTool, textualScore, numericalScore, imageStatement, textStatement}

abstract class Element{

  ElementType getType();
}
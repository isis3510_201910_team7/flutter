import 'package:scoped_model/scoped_model.dart';
import 'package:psycotest/models/TestEditor/TestStructure.dart';
import 'package:json_annotation/json_annotation.dart';

part 'TestEditor.g.dart';

@JsonSerializable()
class TestEditor extends Model {
  //------------------------------------------------------------
  //                 Attributes
  //------------------------------------------------------------

  List<TestStructure> tests;
  int currentTestIndex;

  /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory TestEditor.fromJson(Map<String, dynamic> json) => _$TestEditorFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$TestEditorToJson(this);
  //------------------------------------------------------------
  //                 Builder
  //------------------------------------------------------------

  TestEditor() {
    tests = new List();
  }

  //------------------------------------------------------------
  //                 Getters and Setters
  //------------------------------------------------------------

  TestStructure getCurrentTestStructure() {
    return tests[currentTestIndex];
  }

  void setCurrentTestStructureName(String name) {
    tests[currentTestIndex].name = name;
  }

  void setCurrentTestStructureDescription(String description) {
    tests[currentTestIndex].description = description;
  }

  void setCurrentTestStructureInstructions(String instructions) {
    tests[currentTestIndex].instructions = instructions;
  }

  //------------------------------------------------------------
  //                 Methods
  //------------------------------------------------------------
  void createTestStructure(String name, String description, [String instructions]) {
    TestStructure _newTest = new TestStructure(name, description, instructions);
    tests.add(_newTest);
    currentTestIndex = tests.length - 1;
  }

  void deleteTests(){
    tests = new List();
  }
}

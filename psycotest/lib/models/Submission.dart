import './Examinee.dart';
import './Protocol.dart';
import './TestEditor/TestStructure.dart';
import 'package:json_annotation/json_annotation.dart';

part 'Submission.g.dart';

@JsonSerializable()
class Submission {
  DateTime date;
  List<TestStructure> tests = new List();
  List<String> answers = new List();

    /// A necessary factory constructor for creating a new UserModel instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory Submission.fromJson(Map<String, dynamic> json) =>
      _$SubmissionFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$SubmissionToJson(this);

  //List<Answer> answers;

  Submission(this.date,this.tests);
}

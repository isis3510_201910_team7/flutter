import 'dart:convert';
import 'dart:io';

import 'package:firebase_database/firebase_database.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:psycotest/models/Submission.dart';
import 'package:psycotest/models/TestEditor/TestEditor.dart';
import 'package:psycotest/models/TestEditor/TestStructure.dart';
import 'package:scoped_model/scoped_model.dart';

import './Examinee.dart';
import './Protocol.dart';

/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'UserModel.g.dart';
/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.


/// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.

@JsonSerializable()
class UserModel extends Model {
  List<Examinee> examinees;
  List protocols;
  List submissions;
  List<TestStructure> tests;
  List<TestStructure> currentTestsSelected = new List();
  Protocol currentProtocol;
  Examinee currentExaminee;
  Submission currentSubmission;
  TestEditor editor;


  /// A necessary factory constructor for creating a new UserModel instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$UserModelToJson(this);

  UserModel() {
    loginUser();
  }

  void loginUser() {
    editor = new TestEditor();
    examinees = new List<Examinee>();
    protocols = new List<Protocol>();
    submissions = new List();
    tests = new List();
    currentProtocol = null;
    currentExaminee = null;
    currentSubmission = null;
  }

  getTestsFromFirebase(DataSnapshot snapshot){
    Map a = new Map<String, dynamic>.from(snapshot.value);

    try{
      a.values.forEach((value){
        Map map = Map.from(value);
        TestStructure newTest = TestStructure.fromMapDirect(map);
        tests.add(newTest);
      });
    } catch (e){
      print("User model - tests: error -" + e.toString());
    }
    
  }

  void getExamineesFromFirebase(DataSnapshot snapshot) {
    Map a = new Map<String, dynamic>.from(snapshot.value);
    try{
      a.forEach((id,value){
        Map b = new Map<String, dynamic>.from(value);
        Examinee newExaminee = Examinee.fromMap(b, id);
        examinees.add(newExaminee);
      });
    } catch (e){
      print("User model - examinees: error -" + e.toString());
    }
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/user.json');
  }

  Future<File> saveAsJSON() async {
    final file = await _localFile;

    // Write the file
    String jsonString = jsonEncode(this);
    return file.writeAsString(jsonString);
  }

  
}

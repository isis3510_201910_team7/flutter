// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'UserModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModel _$UserModelFromJson(Map<String, dynamic> json) {
  return UserModel()
    ..examinees = (json['examinees'] as List)
        ?.map((e) =>
            e == null ? null : Examinee.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..protocols = json['protocols'] as List
    ..submissions = json['submissions'] as List
    ..tests = (json['tests'] as List)
        ?.map((e) => e == null
            ? null
            : TestStructure.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..currentTestsSelected = (json['currentTestsSelected'] as List)
        ?.map((e) => e == null
            ? null
            : TestStructure.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..currentProtocol = json['currentProtocol'] == null
        ? null
        : Protocol.fromJson(json['currentProtocol'] as Map<String, dynamic>)
    ..currentExaminee = json['currentExaminee'] == null
        ? null
        : Examinee.fromJson(json['currentExaminee'] as Map<String, dynamic>)
    ..currentSubmission = json['currentSubmission'] == null
        ? null
        : Submission.fromJson(json['currentSubmission'] as Map<String, dynamic>)
    ..editor = json['editor'] == null
        ? null
        : TestEditor.fromJson(json['editor'] as Map<String, dynamic>);
}

Map<String, dynamic> _$UserModelToJson(UserModel instance) => <String, dynamic>{
      'examinees': instance.examinees,
      'protocols': instance.protocols,
      'submissions': instance.submissions,
      'tests': instance.tests,
      'currentTestsSelected': instance.currentTestsSelected,
      'currentProtocol': instance.currentProtocol,
      'currentExaminee': instance.currentExaminee,
      'currentSubmission': instance.currentSubmission,
      'editor': instance.editor
    };

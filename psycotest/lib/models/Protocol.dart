import './TestEditor/TestStructure.dart';
import 'package:json_annotation/json_annotation.dart';

import 'package:firebase_database/firebase_database.dart';
part 'Protocol.g.dart';

@JsonSerializable()
class Protocol {
  String name;
  String description;
  String id;
  List<TestStructure> tests = [];

      /// A necessary factory constructor for creating a new User instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory Protocol.fromJson(Map<String, dynamic> json) =>
      _$ProtocolFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$ProtocolToJson(this);

  Protocol.fromMap(Map<String, dynamic> map) {
    description = map['description'];
    id = map['id'];
    name = map['name'];
    Map<String,dynamic> a = Map<String,dynamic>.from(map['Tests'] );
    try{
    a.forEach((id,test) {
      Map b = new Map<String, dynamic>.from(test);
      TestStructure newTest = TestStructure.fromMapDirect(b);
      tests.add(newTest);      
    });
    } catch (e){
      print('Protocol: '+e.toString());
    }
  }
      

  Protocol.fromSnapshot(DataSnapshot snapshot)
      : this.fromMap(new Map<String, dynamic>.from(snapshot.value));

  Protocol(this.name, this.tests,this.description);
}

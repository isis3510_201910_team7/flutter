import 'package:json_annotation/json_annotation.dart';
import 'package:firebase_database/firebase_database.dart';

// This allows the `User` class to access private members in
/// the generated file. The value for this is *.g.dart, where
/// the star denotes the source file name.
part 'Examinee.g.dart';

@JsonSerializable()
class Examinee {
  String name;
  String education;
  String gender;
  String id;
  String birthdate;
  String key;

  /// A necessary factory constructor for creating a new UserModel instance
  /// from a map. Pass the map to the generated `_$UserFromJson()` constructor.
  /// The constructor is named after the source class, in this case User.
  factory Examinee.fromJson(Map<String, dynamic> json) => _$ExamineeFromJson(json);

  /// `toJson` is the convention for a class to declare support for serialization
  /// to JSON. The implementation simply calls the private, generated
  /// helper method `_$UserToJson`.
  Map<String, dynamic> toJson() => _$ExamineeToJson(this);

   Examinee.fromMap(Map<String, dynamic> map, String key)
    {
       name = map['name'];
       education = map['education'];
       gender = map['gender'];
       id = map['id'];
       birthdate = map['birthdate'];
       this.key = key;
}
       

 Examinee.fromSnapshot(DataSnapshot snapshot)
     : this.fromMap(new Map<String, dynamic>.from(snapshot.value), snapshot.key);

  Examinee(this.name, this.education, this.gender, this.id, this.birthdate);

}
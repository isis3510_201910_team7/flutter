// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Examinee.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Examinee _$ExamineeFromJson(Map<String, dynamic> json) {
  return Examinee(
      json['name'] as String,
      json['education'] as String,
      json['gender'] as String,
      json['id'] as String,
      json['birthdate'] as String)
    ..key = json['key'] as String;
}

Map<String, dynamic> _$ExamineeToJson(Examinee instance) => <String, dynamic>{
      'name': instance.name,
      'education': instance.education,
      'gender': instance.gender,
      'id': instance.id,
      'birthdate': instance.birthdate,
      'key': instance.key
    };

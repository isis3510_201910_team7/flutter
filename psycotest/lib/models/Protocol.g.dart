// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Protocol.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Protocol _$ProtocolFromJson(Map<String, dynamic> json) {
  return Protocol(
      json['name'] as String,
      (json['tests'] as List)
          ?.map((e) => e == null
              ? null
              : TestStructure.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      json['description'] as String)
    ..id = json['id'] as String;
}

Map<String, dynamic> _$ProtocolToJson(Protocol instance) => <String, dynamic>{
      'name': instance.name,
      'description': instance.description,
      'id': instance.id,
      'tests': instance.tests
    };

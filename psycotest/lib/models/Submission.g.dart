// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Submission.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Submission _$SubmissionFromJson(Map<String, dynamic> json) {
  return Submission(
      json['date'] == null ? null : DateTime.parse(json['date'] as String),
      (json['tests'] as List)
          ?.map((e) => e == null
              ? null
              : TestStructure.fromJson(e as Map<String, dynamic>))
          ?.toList())
    ..answers = (json['answers'] as List)?.map((e) => e as String)?.toList();
}

Map<String, dynamic> _$SubmissionToJson(Submission instance) =>
    <String, dynamic>{
      'date': instance.date?.toIso8601String(),
      'tests': instance.tests,
      'answers': instance.answers
    };

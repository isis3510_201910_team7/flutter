import 'dart:convert';
import 'dart:io';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:psycotest/views/CreateExamineeScreen/CreateExaminee.dart';
import 'package:psycotest/views/CreateProtocolScreen/CreateProtocol.dart';
import 'package:psycotest/views/TestEditor/CreateTest.dart';
import 'package:scoped_model/scoped_model.dart';

import './models/UserModel.dart';
import './resources/strings.dart';
import './routes/route.dart';
import './views/ExamineesList.dart';
import './views/ProtocolsList.dart';
import './views/TestsList.dart';

void main() async {
  try {
    final UserModel user = await loadUserJSON();
    FirebaseDatabase database = new FirebaseDatabase();
    database.setPersistenceEnabled(true);
    database.setPersistenceCacheSizeBytes(10000000);
    final databaseReference = FirebaseDatabase.instance.reference();
    databaseReference.child('tests').once().then((snapshot) {
      user.getTestsFromFirebase(snapshot);
    });
    databaseReference.child('pacientes').once().then((snapshot){
      user.getExamineesFromFirebase(snapshot);
    });
    runApp(
      ScopedModel<UserModel>(
        model: user,
        child: HomeRoute(),
      ),
    );
  } on Exception catch (ex) {
    print('Query error: $ex');
  }
}

Future<String> get _localPath async {
  final directory = await getApplicationDocumentsDirectory();

  return directory.path;
}

Future<File> get _localFile async {
  final path = await _localPath;
  return File('$path/user.json');
}

Future<UserModel> loadUserJSON() async {
  try {
    final file = await _localFile;

    // Read the file
    String contents = await file.readAsString();
    Map userMap = jsonDecode(contents);
    return UserModel.fromJson(userMap);
  } catch (e) {
    // If encountering an error, return 0
    return new UserModel();
  }
}

class MyApp extends StatelessWidget {
  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer =
      FirebaseAnalyticsObserver(analytics: analytics);

// This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: Strings.APP_NAME,
        theme: ThemeData(
          primarySwatch: Colors.deepOrange,
        ),
        home: HomeScreen(
          analytics: analytics,
          observer: observer,
        ));
  }
}

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key, this.title, this.analytics, this.observer})
      : super(key: key);

  final String title;
  final FirebaseAnalytics analytics;
  final FirebaseAnalyticsObserver observer;

  @override
  State<StatefulWidget> createState() => HomeScreenState(analytics, observer);
}

class HomeScreenState extends State<HomeScreen> {
  HomeScreenState(this.analytics, this.observer);

  int _currentTab = 0;

  final FirebaseAnalyticsObserver observer;
  final FirebaseAnalytics analytics;

  void _tabChanged(int index) {
    setState(() {
      _currentTab = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
          appBar: AppBar(
            title: Text(Strings.APP_NAME),
            bottom: TabBar(
              tabs: [
                TabItem(Strings.PROTOCOLS),
                TabItem(Strings.TESTS),
                TabItem(Strings.EXAMINEES),
              ],
              onTap: _tabChanged,
            ),
          ),
          drawer: Drawer(),
          body: TabBarView(
            children: [
              ProtocolsList(),
              TestsList(),
              ExamineesList(),
            ],
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Widget destination = CreateProtocol();
              if (_currentTab == 1) {
                destination = CreateTest();
              } else if (_currentTab == 2)
                destination = CreateExaminee();
              else
                destination = CreateProtocol();
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => destination),
              );
            },
            child: Icon(Icons.add),
            backgroundColor: Colors.blueAccent,
          )),
    );
  }
}

class TabItem extends StatelessWidget {
  final String text;

  TabItem(this.text);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 16.0),
        child: Text(
          text,
          style: TextStyle(
            fontSize: 16.0,
          ),
        ));
  }
}

class StringFormat {
  static String formatDate(String date) {
    String response = date;
    try {
      response = response.split(" ")[0].replaceAll("-", "/");
      var dates = response.split("/");
      response = dates[2] + "/" + dates[1] + "/" + dates[0];
    } catch (e) {
      print("error formating date - " + e.toString());
    }

    return response;
  }
}
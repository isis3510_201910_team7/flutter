import 'package:flutter/material.dart';
class AlertMaker {
    static void showAlertDialog(BuildContext context, String message) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Ups! Algo ha pasado"),
            content: Text(message),
            actions: <Widget>[
              new FlatButton(
                child: Text("Cerrar"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }
}

class Strings {
  static const String APP_NAME = "Psycotest";
  static const String PROTOCOLS = "Protocolos";
  static const String TESTS = "Tests";
  static const String EXAMINEES = "Examinados";
  static const String PROTOCOL = "Protocolo";
  static const String TEST = "Prueba";
  static const String EXAMINEE = "Examinado";
  static const String CREATE_SOMETHING = "Crear %s";
  static const String APPLY_SOMETHING = "Aplicar %s";
  static const GENDER_MALE = "Masculino";
  static const GENDER_FEMALE = "Femenino";
  static const GENDER_OTHER = "Otro";
  static const CREATE = "Crear";

  static const NEW_TEST = "Nueva Prueba";
  static const TEST_NAME_TEXT_FIELD_HINT = 'Nombre de la prueba';
  static const TEST_DESCRIPTION_TEXT_FIELD_HINT = 'Descripción de la prueba';
  static const TEST_INSTRUCTION_TEXT_FIELD_HINT = 'Reglas y remonedaciones generales para aplicar la prueba';


  static const NAME_LABEL = "Nombre";
  static const DESCRIPTION_LABEL = "Descripción";
  static const INSTRUCTION_LABEL = "Instrucciones";
  static const DONE = "Hecho";
  static const ADD_ELEMENT_TOOLTIP = "Agregar elemento";
  static const INSERT_SOME_TEXT = "porfavor completa el campo.";
  static const CREATE_TEST = "Crear prueba";
  static const VOID_INSTRUCTIONS = "Esta prueba no tiene instrucciones para su realización.";


  //CREATE EXAMINEE SCREEN 
  static const EXAMINEE_NAME_HINT = "Nombre completo";
  static const EXAMINEE_EDUCATION_HINT = "Educación";
  static const EXAMINEE_GENDER_HINT = "Género";
  static const EXAMINEE_ID_HINT = "Documento de identificación";
  static const EXAMINEE_BIRTHDATE_HINT = "Fecha de nacimiento";

  //CREATE PROTOCOL SCREEN
  static const PROTOCOL_NAME_HINT = "Nombre del protocolo";
  static const PROTOCOL_DESCRIPTION_HINT = "Descripción";
  static const PROTOCOL_TESTS_TITLE = "Tests";

  static Map<String, Map<String, String>> values = {
    'en': {
      'endEditingToolTip': 'Hello World',
    },
    'es': {
      'endEditingToolTip': 'Finalizar edición',
      'editing': 'Editando',
    },
  };

}

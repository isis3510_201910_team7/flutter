class FormValidators {
  static String emptyValidator(String value) {
    if (value.isEmpty) {
      return "El campo no puede estar vacío";
    }
  }
  static String shortStringValidator(String value) {
    if(value.isEmpty){
      return "El campo no puede estar vacío";
    } else if (value.length > 50){
      return "El campo no puede tener mas de 50 caracteres";
    }
  }

  static String mediumStringValidator(String value) {
    if(value.isEmpty){
      return "El campo no puede estar vacío";
    } else if (value.length > 100){
      return "El campo no puede tener mas de 50 caracteres";
    }
  }

  static String longStringValidator(String value) {
    if(value.isEmpty){
      return "El campo no puede estar vacío";
    } else if (value.length > 300){
      return "El campo no puede tener mas de 50 caracteres";
    }
  }
}
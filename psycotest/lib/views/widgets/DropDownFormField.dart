import 'package:flutter/material.dart';

class DropDownFormField extends StatefulWidget {
  final SetFieldCallback onFieldChanged;
  final options;
  final label;

  DropDownFormField({this.onFieldChanged, this.options, this.label});

  @override
  _DropDownFormField createState() => _DropDownFormField();
}

class _DropDownFormField extends State<DropDownFormField> {
  int dropdownValue = 0;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Text(widget.label + ":      .", textAlign: TextAlign.right,),
          flex: 10,
        ),
        Expanded(
          child: DropdownButton<String>(
            value: widget.options[dropdownValue],
            onChanged: (String newValue) {
              setState(() {
                widget.onFieldChanged(newValue);
                for (int i = 0; i < widget.options.length; i++) {
                  if (widget.options[i] == newValue) dropdownValue = i;
                }
              });
            },
            items: widget.options.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),
          flex: 4,
        ),
        Expanded(
          child: Container(),
          flex: 6,
        ),
      ],
    );
  }
}

typedef SetFieldCallback = void Function(String option);

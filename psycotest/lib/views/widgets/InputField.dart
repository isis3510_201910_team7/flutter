import 'package:flutter/material.dart';
class InputField extends StatelessWidget {
  final validator;
  final hint;
  final label;
  final type;
  final onsave;
  final maxLength;

  InputField({this.validator, this.hint, this.label, this.type, this.onsave, this.maxLength});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(bottom: 16.0),
        child: TextFormField(
          onSaved: onsave,
          keyboardType: type,
          maxLength: maxLength,
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            labelText: label,
            hintText: hint,
          ),
          validator: validator,
        ));
  }
}

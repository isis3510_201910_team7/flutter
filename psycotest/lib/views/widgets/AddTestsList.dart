import 'package:flutter/material.dart';

import '../../models/TestEditor/TestStructure.dart';

class AddTestList extends StatelessWidget {
  final List elements;
  final changeElement;

  AddTestList({this.elements, this.changeElement});

  Widget _buildList() {
    if (elements == null) {
      return null;
    } else {
      var aux = elements.where((element) => element!=null && element.name!=null);
      return ListView.builder(
        itemCount: aux.length,
        itemBuilder: (BuildContext context, int index) {
          return new ProtocolTestItem(aux.elementAt(index), changeElement);
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      color: Colors.grey.shade200,
      height: 250.0,
      child: _buildList(),
    );
  }
}

class ProtocolTestItem extends StatefulWidget {
  final changeTestList;
  final TestStructure test;

  ProtocolTestItem(this.test, this.changeTestList);

  @override
  State<StatefulWidget> createState() =>
      ProtocolTestItemState(test, changeTestList);
}

class ProtocolTestItemState extends State<ProtocolTestItem> {
  TestStructure test;
  final changeTestList;
  bool selected = false;

  ProtocolTestItemState(this.test, this.changeTestList);

  @override
  Widget build(BuildContext context) {
    if (selected) {
      return Container(
        child: FlatButton.icon(
          icon: Icon(Icons.check, color: Colors.orange),
          label: Text(
            test.name,
            style: TextStyle(color: Colors.orange),
          ),
          onPressed: () {
            this.setState(() => selected = false);
            changeTestList(test, selected);
          },
        ),
        alignment: Alignment.centerLeft,
      );
    } else {
      return Container(
        child: FlatButton.icon(
            icon: Icon(Icons.add),
            label: Text(test.name),
            onPressed: () {
              this.setState(() => selected = true);
              changeTestList(test, selected);
            }),
        alignment: Alignment.centerLeft,
      );
    }
  }
}

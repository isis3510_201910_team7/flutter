import 'package:flutter/material.dart';

class LongTextFormField extends StatefulWidget {

  final SetFieldCallback onFieldValidated;
  final hint;
  final label;
  final initialValue;
  final required;

  LongTextFormField({this.onFieldValidated, this.hint, this.label, this.required, this.initialValue});

  @override
  _LongTextFormField createState() => _LongTextFormField();
}

class _LongTextFormField extends State<LongTextFormField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      maxLines: 5,
      textInputAction: TextInputAction.done,
      textCapitalization: TextCapitalization.sentences,
      textAlign: TextAlign.left,
      initialValue: widget.initialValue,
      decoration: InputDecoration(
        hintText: widget.hint,
        labelText: widget.label,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(2.0),
        ),
      ),
      validator: (value) {
        if(widget.required){
          if (value.isEmpty) return "Por favor llena este campo";
        }
        else if (value.length > 200) return "Este campo no puede tener más de 200 carácteres.";
      },
      onSaved: (value) {
        widget.onFieldValidated(value);
      },
    );
  }
}

typedef SetFieldCallback = void Function(String text);
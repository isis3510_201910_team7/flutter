import 'package:flutter/material.dart';
class FloatingActionButtonApplication extends StatelessWidget {

  final active;
  final onPressed;

  FloatingActionButtonApplication({this.active,this.onPressed});

  @override
  Widget build(BuildContext context) {
    if(!active){
      return new FloatingActionButton(
          onPressed: null,
          child: Icon(
            Icons.play_arrow,
            color: Colors.white,
          ),
          backgroundColor: Colors.grey,
        );
    } else {
      return new FloatingActionButton(
          onPressed: this.onPressed,       
          child: Icon(
            Icons.play_arrow,
            color: Colors.white,
          ),
          backgroundColor: Colors.blueAccent,
        );
    }
  }

}
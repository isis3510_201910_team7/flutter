import 'package:flutter/material.dart';

class ElementLabel extends StatelessWidget{
  final onPressed;
  final index;
  final name;
  final description;
  final isContainer;

  ElementLabel({this.onPressed, this.index, this.name, this.description, this.isContainer});

  getIcon(){
    if(isContainer)
      return Icon(Icons.chevron_right);
    else
      return Icon(Icons.edit);
  }

  String fixDescription(){
    if(description.length > 25){
      return description.substring(0,25) + " ...";
    }
    return description;
  }
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: Colors.white,
      padding: EdgeInsets.all(10.0),
      onPressed: () {
        onPressed(index);
      },
      shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(20.0)),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(),
          ),
          Expanded(
            flex: 18,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: Text(
                    name,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      height: 2.0,
                    ),
                  ),
                ),
                Expanded(
                  flex: 4,
                  child: Text(
                    fixDescription(),
                    style: TextStyle(
                        fontStyle: FontStyle.italic,
                        color: Colors.grey),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: getIcon(),
          ),
        ],
      ),
    );
  }
}
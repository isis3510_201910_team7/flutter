import 'package:flutter/material.dart';

class ShortTextFormField extends StatefulWidget {

  final SetFieldCallback onFieldValidated;
  final hint;
  final label;
  final initialValue;

  ShortTextFormField({this.onFieldValidated, this.hint, this.label, this.initialValue});

  @override
  _ShortTextFormField createState() => _ShortTextFormField();
}

class _ShortTextFormField extends State<ShortTextFormField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: widget.initialValue,
      textAlign: TextAlign.center,
      decoration: InputDecoration(
        hintText: widget.hint,
        labelText: widget.label,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(2.0),
        ),
      ),
      validator: (value) {
        if (value.isEmpty) return "Por favor llena este campo";
        else if (value.length > 10) return "Este campo no puede tener más de 10 carácteres.";
      },
      onSaved: (value) {
        widget.onFieldValidated(value);
      },
    );
  }
}

typedef SetFieldCallback = void Function(String text);
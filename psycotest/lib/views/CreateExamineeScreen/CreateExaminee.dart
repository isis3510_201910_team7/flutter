import 'package:flutter/material.dart';
import 'package:sprintf/sprintf.dart';

import './ExamineeForm.dart';
import '../../resources/strings.dart';

class CreateExaminee extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title:
          Text(sprintf(Strings.CREATE_SOMETHING, [Strings.EXAMINEE])),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(null),
          ),
        ),
        body: Padding(
          padding: EdgeInsets.all(16.0),
          child: ExamineeForm(),
        ));
  }
}

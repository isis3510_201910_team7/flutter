import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:psycotest/models/UserModel.dart';
import 'package:psycotest/resources/StringFormat.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../resources/FormValidators.dart';
import '../widgets/InputField.dart';
import '../../resources/strings.dart';
import 'package:firebase_database/firebase_database.dart';

class ExamineeForm extends StatefulWidget {
  @override
  ExamineeFormState createState() {
    return ExamineeFormState();
  }
}

class ExamineeFormState extends State<ExamineeForm> {
  final _formKey = GlobalKey<FormState>();
  String _name;
  String _education;
  String _gender;
  String _id;
  String _birthdate = StringFormat.formatDate(DateTime.now().toString());

  Future _selectDate() async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: new DateTime(1990),
        firstDate: new DateTime(1900),
        lastDate: new DateTime(2019));
    if (picked != null)
      setState(() => _birthdate = StringFormat.formatDate(picked.toString()));
  }

  

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child:
            ScopedModelDescendant<UserModel>(builder: (context, child, model) {
          return ListView(
            children: <Widget>[
              InputField(
                label: Strings.EXAMINEE_NAME_HINT,
                validator: FormValidators.shortStringValidator,
                onsave: (value) => this._name = value,
                maxLength: 50,
              ),
              InputField(
                label: Strings.EXAMINEE_EDUCATION_HINT,
                validator: FormValidators.shortStringValidator,
                type: TextInputType.text,
                onsave: (value) => this._education = value,
                maxLength: 50,
              ),
              Container(
                  margin: EdgeInsets.only(bottom: 16.0),
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0),
                    border: Border.all(
                        color: Colors.black54,
                        style: BorderStyle.solid,
                        width: 1.0),
                  ),
                  child: DropdownButton(
                    isExpanded: true,
                    value: this._gender,
                    items: <DropdownMenuItem>[
                      DropdownMenuItem(
                          child: Text(Strings.GENDER_MALE),
                          value: Strings.GENDER_MALE),
                      DropdownMenuItem(
                          child: Text(Strings.GENDER_FEMALE),
                          value: Strings.GENDER_FEMALE),
                      DropdownMenuItem(
                          child: Text(Strings.GENDER_OTHER),
                          value: Strings.GENDER_OTHER),
                    ],
                    onChanged: (value) {
                      this._gender = value;
                      setState(() {
                        this._gender;
                      });
                    },
                    hint: new Text(Strings.EXAMINEE_GENDER_HINT),
                  )),
              InputField(
                label: Strings.EXAMINEE_ID_HINT,
                validator: FormValidators.shortStringValidator,
                onsave: (value) => this._id = value,
                maxLength: 50,
              ),
              Container(
                padding: new EdgeInsets.all(8.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  border: Border.all(
                      color: Colors.orange,
                      style: BorderStyle.solid,
                      width: 2.0),
                ),
                child: new Center(
                    child: new FlatButton(
                  onPressed: _selectDate,
                  child: new Text(
                      Strings.EXAMINEE_BIRTHDATE_HINT + ": " + _birthdate),
                  color: Colors.transparent,
                )),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: RaisedButton(
                  padding: EdgeInsets.all(12.0),
                  color: Colors.deepOrange,
                  textColor: Colors.white,
                  onPressed: () {
                    // Validate will return true if the form is valid, or false if
                    // the form is invalid.
                    if (_formKey.currentState.validate()) {
                      _formKey.currentState.save();
                      FirebaseDatabase.instance
                          .reference()
                          .child('pacientes')
                          .push()
                          .set({
                        "name": _name,
                        "education": _education,
                        "gender": _gender,
                        "birthdate": _birthdate,
                        "id": _id
                      }).then((result) {
                        Navigator.of(context).pop(null);
                      }).catchError((err) =>
                              Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text('Hubo un error'),
                                duration: Duration(seconds: 3),
                              )));
                    }
                  },
                  child: Text(
                    'Crear',
                    style: TextStyle(fontSize: 22.0),
                  ),
                ),
              ),
            ],
          );
        }));
  }
}

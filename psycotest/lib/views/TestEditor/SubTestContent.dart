import 'dart:io';

import 'package:flutter/material.dart';
import 'package:psycotest/models/TestEditor/MessageStructure.dart';
import 'package:psycotest/models/TestEditor/SubTestStructure.dart';
import 'package:psycotest/models/TestEditor/SuperClasses/TestElement.dart';
import 'package:psycotest/models/UserModel.dart';
import 'package:psycotest/resources/strings.dart';
import 'package:psycotest/views/TestEditor/CreateItem.dart';
import 'package:psycotest/views/TestEditor/CreateMessage.dart';
import 'package:psycotest/views/TestEditor/CreateSubTest.dart';
import 'package:psycotest/views/TestEditor/EditMessage.dart';
import 'package:psycotest/views/widgets/ElementLabel.dart';
import 'package:psycotest/views/widgets/FabWithIcons.dart';
import 'package:scoped_model/scoped_model.dart';

class SubTestContent extends StatefulWidget {
  final SubTestStructure subTest;

  SubTestContent({this.subTest});
  @override
  _SubTestContent createState() => _SubTestContent();
}

class _SubTestContent extends State<SubTestContent> {
  final icons = [Icons.message, Icons.pie_chart];
  final labels = ["mensaje", "item"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () => Navigator.of(context).pop(null),
            );
          },
        ),
        title: ScopedModelDescendant<UserModel>(
          builder: (context, child, user) {
            return Text(
              widget.subTest.name + "(Sub-prueba)"
            );
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.edit),
            tooltip: Strings.DONE,
            onPressed: () {
              //TODO: Pintar la ventana de editar
            },
          ),
        ],
      ),
      body: SubTestsElementsList(subTest: widget.subTest),
      floatingActionButton: FabWithIcons(
        icons: icons,
        labels: labels,
        onIconTapped: (index) {
          if (index == 0)
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => CreateMessage(parent: widget.subTest)),
            );
          else if (index == 1) {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => CreateItem(parent: widget.subTest)),
            );
          }
          ScopedModel.of<UserModel>(context).saveAsJSON();
        },
      ),
      //_buildFab(context),
      /*floatingActionButton: FloatingActionButton(
        onPressed: () => setState(() {
              //TODO: Floating action button options
            }),
        tooltip: Strings.ADD_ELEMENT_TOOLTIP,
        child: Icon(Icons.add),
      ),*/
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}

class SubTestsElementsList extends StatelessWidget {
  final subTest;

  SubTestsElementsList({this.subTest});
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blueGrey[50],
      child: ScopedModelDescendant<UserModel>(builder: (context, child, user) {
        return ListView.builder(
          padding: EdgeInsets.all(20.0),
          itemExtent: 100,
          itemCount: subTest.elements.length,
          itemBuilder: (context, index) {
            final _element =
            subTest.elements[index];
            final _index = index;
            switch(_element.getType()){
              case ElementType.message:
                MessageStructure _message = _element;
                return ElementLabel(
                  isContainer: false,
                  index: _index,
                  name: "Mensaje",
                  description: _message.text,
                  onPressed: (int index){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => EditMessage(messageStructure: _message)),
                    );
                  },
                );
                break;
              default:
                return Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: RaisedButton(
                    color: Colors.white,
                    padding: EdgeInsets.all(10.0),
                    onPressed: () {},
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(20.0)),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Container(),
                        ),
                        Expanded(
                          flex: 18,
                          child: Text(
                            _index.toString() +
                                "." +
                                _element.runtimeType.toString(),
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              height: 2.0,
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Icon(Icons.chevron_right),
                        ),
                      ],
                    ),
                  ),
                );
                break;
            }
          },
        );
      }),
    );
  }
}

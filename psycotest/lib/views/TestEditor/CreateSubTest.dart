import 'dart:io';

import 'package:flutter/material.dart';
import 'package:psycotest/views/widgets/LongTextFormField.dart';
import 'package:psycotest/views/widgets/ShortTextFormField.dart';

import 'package:scoped_model/scoped_model.dart';
import 'package:psycotest/models/UserModel.dart';
import 'package:psycotest/resources/strings.dart';

class CreateSubTest extends StatefulWidget {
  @override
  _CreateSubTest createState() => _CreateSubTest();
}

class _CreateSubTest extends State<CreateSubTest> {
  final _formKey = GlobalKey<FormState>();

  String _name;
  String _description;
  String _instruction;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () => Navigator.of(context).pop(null),
              );
            },
          ),
          title: Text(Strings.NEW_TEST),
        ),
        body: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 30.0),
                child: ShortTextFormField(
                  label: "Nombre*",
                  hint: "nombre de la sub-prueba",
                  onFieldValidated: (String value) {
                    _name = value;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(30.0),
                child: LongTextFormField(
                  required: true,
                  label: "Descripción*",
                  hint: "breve descripción de la sub-prueba",
                  onFieldValidated: (String value) {
                    _description = value;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(30.0),
                child: LongTextFormField(
                  required: false,
                  label: "Instrucciones",
                  hint: "Recomendaciones para la realización de la sub-prueba",
                  onFieldValidated: (String value) {
                    _instruction = value;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(30, 40, 30, 30),
                child: FractionallySizedBox(
                  widthFactor: 0.2,
                  child: RaisedButton(
                    color: Colors.blueAccent,
                    textColor: Colors.white,
                    onPressed: () {
                      // Validate will return true if the form is valid, or false if
                      // the form is invalid.
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();
                        if(_instruction != null){
                          ScopedModel.of<UserModel>(context)
                              .editor
                              .getCurrentTestStructure()
                              .addSubTestElement(_name, _description, _instruction);
                        }
                        else{
                          ScopedModel.of<UserModel>(context)
                              .editor
                              .getCurrentTestStructure()
                              .addSubTestElement(_name, _description);
                        }
                        // If the form is valid, display a snackbar. In the real world, you'd
                        // often want to call a server or save the information in a database
                        Future<File> ans =
                            ScopedModel.of<UserModel>(context).saveAsJSON();
                        Navigator.pop(context);
                      }
                    },
                    child: Text("crear sub-test"),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}



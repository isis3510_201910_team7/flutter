import 'dart:io';

///import 'dart:io';

import 'package:flutter/material.dart';
import 'package:psycotest/widgets/textEditor.dart';

//import 'package:scoped_model/scoped_model.dart';
//import 'package:psycotest/models/UserModel.dart';
import 'package:psycotest/resources/strings.dart';

class CreateTest extends StatefulWidget {
  @override
  _CreateTest createState() => _CreateTest();
}

class _CreateTest extends State<CreateTest> {
  final _formKey = GlobalKey<FormState>();

  String _name;
  String _description;
  String _instruction;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () => Navigator.of(context).pop(null),
              );
            },
          ),
          title: Text(Strings.NEW_TEST),
        ),
        body: Form(
          key: _formKey,
          child: Padding(
                padding: const EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 30.0),
                child: TextEditor(
                  onEditingEnds:(String document){
                  },
                ),
          ),/*ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 30.0),
                child: TestName(
                  onNameValidated: (String name) {
                    _name = name;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(30.0),
                child: TestDescription(
                  onDescriptionValidated: (String description) {
                    _description = description;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(30.0),
                child: TestInstruction(
                  onInstructionValidated: (String instruction) {
                    _instruction = instruction;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(30, 40, 30, 30),
                child: FractionallySizedBox(
                  widthFactor: 0.2,
                  child: RaisedButton(
                    color: Colors.blueAccent,
                    textColor: Colors.white,
                    onPressed: () {
                      // Validate will return true if the form is valid, or false if
                      // the form is invalid.
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();
                        if (_instruction != null)
                          ScopedModel.of<UserModel>(context)
                              .editor
                              .createTestStructure(
                                  _name, _description, _instruction);
                        else
                          ScopedModel.of<UserModel>(context)
                              .editor
                              .createTestStructure(
                                _name,
                                _description,
                              );
                        // If the form is valid, display a snackbar. In the real world, you'd
                        // often want to call a server or save the information in a database
                        Future<File> ans =
                        ScopedModel.of<UserModel>(context).saveAsJSON();
                        Navigator.pop(context);
                      }
                    },
                    child: Text(Strings.CREATE_TEST),
                  ),
                ),
              ),
            ],
          ),*/
        ));
  }
}

class TestName extends StatefulWidget {
  TestName({this.onNameValidated});

  final SetNameCallback onNameValidated;

  @override
  _TestName createState() => _TestName();
}

class _TestName extends State<TestName> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textAlign: TextAlign.center,
      decoration: InputDecoration(
        hintText: Strings.TEST_NAME_TEXT_FIELD_HINT,
        labelText: Strings.NAME_LABEL + ' *',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(2.0),
        ),
      ),
      validator: (value) {
        if (value.isEmpty) return Strings.INSERT_SOME_TEXT;
      },
      onSaved: (value) {
        widget.onNameValidated(value);
      },
    );
  }
}

typedef SetNameCallback = void Function(String name);

class TestDescription extends StatefulWidget {
  TestDescription({this.onDescriptionValidated});

  final SetDescriptionCallback onDescriptionValidated;

  @override
  _TestDescription createState() => _TestDescription();
}

class _TestDescription extends State<TestDescription> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      maxLines: 3,
      textInputAction: TextInputAction.done,
      textCapitalization: TextCapitalization.sentences,
      textAlign: TextAlign.left,
      decoration: InputDecoration(
        hintText: Strings.TEST_DESCRIPTION_TEXT_FIELD_HINT,
        labelText: Strings.DESCRIPTION_LABEL + ' *',
      ),
      validator: (value) {
        if (value.isEmpty) return Strings.INSERT_SOME_TEXT;
      },
      onSaved: (value) {
        widget.onDescriptionValidated(value);
      },
    );
  }
}

typedef SetDescriptionCallback = void Function(String description);

class TestInstruction extends StatefulWidget {
  TestInstruction({this.onInstructionValidated});

  final SetInstructionCallback onInstructionValidated;

  @override
  _TestInstruction createState() => _TestInstruction();
}

class _TestInstruction extends State<TestInstruction> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      maxLines: 5,
      textInputAction: TextInputAction.done,
      textCapitalization: TextCapitalization.sentences,
      textAlign: TextAlign.left,
      decoration: InputDecoration(
        hintText: Strings.TEST_INSTRUCTION_TEXT_FIELD_HINT,
        labelText: Strings.INSTRUCTION_LABEL,
      ),
      onSaved: (value) {
        widget.onInstructionValidated(value);
      },
    );
  }
}

typedef SetInstructionCallback = void Function(String instruction);

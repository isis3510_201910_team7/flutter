import 'dart:io';

import 'package:flutter/material.dart';
import 'package:psycotest/models/TestEditor/MessageStructure.dart';
import 'package:psycotest/views/widgets/DropDownFormField.dart';
import 'package:psycotest/views/widgets/LongTextFormField.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:psycotest/models/UserModel.dart';

class CreateMessage extends StatefulWidget {
  final parent;

  CreateMessage({this.parent});

  @override
  _CreateMessage createState() => _CreateMessage();
}

class _CreateMessage extends State<CreateMessage> {
  final _formKey = GlobalKey<FormState>();

  MESSAGE_TYPE _messageType = MESSAGE_TYPE.ALERT;
  String _messageText;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () => Navigator.of(context).pop(null),
              );
            },
          ),
          title: Text("Nuevo Mensaje"),
        ),
        body: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 30.0),
                child: DropDownFormField(
                  label: "Tipo de mensaje",
                  options: ['alerta', 'advertencia', 'información'],
                  onFieldChanged: (String value) {
                    switch(value){
                      case "alerta":
                        _messageType = MESSAGE_TYPE.ALERT;
                        break;
                      case "advertencia":
                        _messageType = MESSAGE_TYPE.WARNING;
                        break;
                      case "información":
                        _messageType = MESSAGE_TYPE.INFORMATION;
                        break;
                    }
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 30.0),
                child: LongTextFormField(
                  required: true,
                  hint: "texto",
                  label: "mensaje",
                  onFieldValidated: (String value) {
                    _messageText = value;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(30, 40, 30, 30),
                child: FractionallySizedBox(
                  widthFactor: 0.2,
                  child: RaisedButton(
                    color: Colors.blueAccent,
                    textColor: Colors.white,
                    onPressed: () {
                      // Validate will return true if the form is valid, or false if
                      // the form is invalid.
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();
                        if(widget.parent != null){
                          widget.parent.addMessageElement(_messageType, _messageText);
                        }
                       else{
                          ScopedModel.of<UserModel>(context)
                              .editor
                              .getCurrentTestStructure()
                              .addMessageElement(_messageType, _messageText);
                        }
                        // If the form is valid, display a snackbar. In the real world, you'd
                        // often want to call a server or save the information in a database
                        Future<File> ans =
                        ScopedModel.of<UserModel>(context).saveAsJSON();
                        Navigator.pop(context);
                      }
                    },
                    child: Text('Crear'),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
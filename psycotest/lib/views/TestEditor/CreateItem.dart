import 'dart:io';

import 'package:flutter/material.dart';
import 'package:psycotest/models/TestEditor/MessageStructure.dart';
import 'package:psycotest/views/widgets/DropDownFormField.dart';
import 'package:psycotest/views/widgets/LongTextFormField.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:psycotest/models/UserModel.dart';

class CreateItem extends StatefulWidget {
  final parent;

  CreateItem({this.parent});

  @override
  _CreateItem createState() => _CreateItem();
}

class _CreateItem extends State<CreateItem> {
  final _formKey = GlobalKey<FormState>();

  bool _withTrials = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () => Navigator.of(context).pop(null),
              );
            },
          ),
          title: Text("Nuevo Item"),
        ),
        body: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(30.0, 30.0, 30.0, 30.0),
                child: DropDownFormField(
                  label: "Tipo de Item",
                  options: ['simple', 'con ensayos'],
                  onFieldChanged: (String value) {
                    switch (value) {
                      case "simple":
                        _withTrials = false;
                        break;
                      case "con ensayos":
                        _withTrials = true;
                        break;
                    }
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(30, 40, 30, 30),
                child: FractionallySizedBox(
                  widthFactor: 0.2,
                  child: RaisedButton(
                    color: Colors.blueAccent,
                    textColor: Colors.white,
                    onPressed: () {
                      // Validate will return true if the form is valid, or false if
                      // the form is invalid.
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();
                        if (widget.parent != null) {
                          if (_withTrials)
                            widget.parent.addComplexItemElement();
                          else
                            widget.parent.addSimpleItemElement();
                        } else {
                          if (_withTrials) {
                            ScopedModel.of<UserModel>(context)
                                .editor
                                .getCurrentTestStructure()
                                .addComplexItemElement();
                          } else {
                            ScopedModel.of<UserModel>(context)
                                .editor
                                .getCurrentTestStructure()
                                .addSimpleItemElement();
                          }
                        }
                        // If the form is valid, display a snackbar. In the real world, you'd
                        // often want to call a server or save the information in a database
                        Future<File> ans =
                            ScopedModel.of<UserModel>(context).saveAsJSON();
                        Navigator.pop(context);
                      }
                    },
                    child: Text('Crear Item'),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}

import 'package:flutter/material.dart';
import 'package:psycotest/views/TestEditor/TestContent.dart';

import 'package:scoped_model/scoped_model.dart';
import '../models/UserModel.dart';

class TestsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blueGrey[50],
      child: ScopedModelDescendant<UserModel>(builder: (context, child, user) {
        return ListView.builder(
          padding: EdgeInsets.all(20.0),
          itemExtent: 100,
          itemCount: user.editor.tests.length,
          itemBuilder: (context, index) {
            final _test = user.editor.tests[index];
            final _index = index;
            return Padding(
              padding: const EdgeInsets.all(10.0),
              child: RaisedButton(
                color: Colors.white,
                padding: EdgeInsets.all(10.0),
                onPressed: () {
                  ScopedModel.of<UserModel>(context).editor.currentTestIndex =
                      _index;
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => TestContent()),
                  );
                },
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(20.0)),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Container(),
                    ),
                    Expanded(
                      flex: 18,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            flex: 6,
                            child: Text(
                              _test.name,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                height: 2.0,
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 4,
                            child: Text(
                              _test.description,
                              style: TextStyle(
                                  fontStyle: FontStyle.italic,
                                  color: Colors.grey),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Icon(Icons.chevron_right),
                    ),
                  ],
                ),
              ),
            );
          },
        );
      }),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:psycotest/models/UserModel.dart';
import 'package:psycotest/resources/AlertMaker.dart';
import 'package:scoped_model/scoped_model.dart';

import '../models/Protocol.dart';
import '../views/ApplyProtocol/ApplyProtocolScreen.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:connectivity/connectivity.dart';

class ProtocolsList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new ProtocolsListState();
}

class ProtocolsListState extends State<ProtocolsList> {
  var _result = false;
  var _subscription;

  @override
  void initState() {
    super.initState();
    (Connectivity().checkConnectivity()).then((connectivityResult) {
      _result = (connectivityResult == ConnectivityResult.wifi ||
          connectivityResult == ConnectivityResult.mobile);
      this.setState(() => {_result});
    });
    _subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      _result = (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile);
      this.setState(() => {_result});
      if (!_result) {
        AlertMaker.showAlertDialog(
            context, "Parece que no tienes una conexión activa a internet!");
      }
    });
  }

  // Be sure to cancel subscription after you are done
  @override
  dispose() {
    super.dispose();
    _subscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    if (_result) {
      return FirebaseAnimatedList(
          query: FirebaseDatabase.instance.reference().child('protocolos'),
          defaultChild: new Center(
            child: CircularProgressIndicator(),
          ),
          itemBuilder: (context, snapshot, animation, index) { 
            return ScopedModelDescendant<UserModel>(
                builder: (context, child, model) {
              var protocol = new Protocol.fromSnapshot(snapshot);
              
              if (model.protocols.firstWhere(
                      (current) => current.id == protocol.id,
                      orElse: () => null) ==
                  null) {
                model.protocols.add(protocol);
              }
              
              return new ProtocolItemCard(protocol, model);
            });
          });
    } else {
      return ScopedModelDescendant<UserModel>(builder: (context, child, model) {
        return new ListView.builder(
          itemCount: model.protocols.length,
          itemBuilder: (BuildContext context, int index) {
            return new ProtocolItemCard(model.protocols[index], model);
          },
        );
      });
    }
  }
}

class ProtocolItemCard extends StatelessWidget {
  final Protocol protocol;
  final UserModel model;

  ProtocolItemCard(this.protocol, this.model);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        margin: EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new ListTile(
              leading: Icon(
                Icons.chrome_reader_mode,
                size: 50.0,
              ),
              title: Container(
                margin: EdgeInsets.only(bottom: 16.0),
                child: Text(
                  protocol.name,
                  style: TextStyle(fontSize: 24.0),
                ),
              ),
              subtitle: Text(
                'protocolo compuesto de ${protocol.tests.length} test${protocol.tests.length != 1 ? "s":""}',
                style: TextStyle(fontSize: 16.0),
              ),
            ),
            ButtonTheme.bar(
              child: ButtonBar(
                children: <Widget>[
                  FlatButton(
                    child: const Text('APLICAR'),
                    onPressed: () {
                      model.currentExaminee = null;
                      model.currentProtocol = protocol;
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ApplyProtocolScreen()),
                      );
                    },
                  ),
                  FlatButton(
                    child: const Text('VER RESULTADOS'),
                    onPressed: () {
                      /* ... */
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:psycotest/models/UserModel.dart';
import 'package:psycotest/resources/strings.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../models/TestEditor/TestStructure.dart';
import '../../resources/FormValidators.dart';
import '../widgets/AddTestsList.dart';
import '../widgets/InputField.dart';
import 'package:firebase_database/firebase_database.dart';

class ProtocolForm extends StatefulWidget {
  @override
  ProtocolFormState createState() {
    return ProtocolFormState();
  }
}

class ProtocolFormState extends State<ProtocolForm> {
  final _formKey = GlobalKey<FormState>();
  String _name;
  String _description;
  List<TestStructure> testsIncluded = new List<TestStructure>();
  List<TestStructure> tests = new List<TestStructure>();
  var testReference = FirebaseDatabase.instance.reference();

  @override
  void initState() {
    super.initState();
  }

  void changeElementInList(TestStructure test, bool add) {
    if (add) {
      this.setState(() => testsIncluded.add(test));
    } else {
      this.setState(() => testsIncluded.remove(test));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child:
            ScopedModelDescendant<UserModel>(builder: (context, child, model) {
          return ListView(children: <Widget>[
            InputField(
              hint: Strings.PROTOCOL_NAME_HINT,
              validator: FormValidators.shortStringValidator,
              onsave: (value) => this._name = value,
              maxLength: 50,
            ),
            InputField(
              hint: Strings.PROTOCOL_DESCRIPTION_HINT,
              validator: FormValidators.mediumStringValidator,
              onsave: (value) => this._description = value,
              maxLength: 200,
            ),
            Padding(
              child: Text(
                Strings.PROTOCOL_TESTS_TITLE,
                style: TextStyle(fontSize: 16.0),
              ),
              padding: EdgeInsets.all(16.0),
            ),
            AddTestList(
                elements: model.tests, changeElement: changeElementInList),
            Padding(
                padding: const EdgeInsets.symmetric(vertical: 26.0),
                child: new RaisedButton(
                    padding: EdgeInsets.all(12.0),
                    color: Colors.deepOrange,
                    textColor: Colors.white,
                    child: Text(Strings.CREATE),
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();
                        final databaseReference =
                            FirebaseDatabase.instance.reference();
                        Map aux = new Map<String, dynamic>();
                        testsIncluded.forEach((test) {
                          aux[test.id] = test.toMap();
                        });
                        databaseReference.child('protocolos').push().set({
                          "name": _name,
                          "descripcion": _description,
                          "Tests": aux,
                        }).then((result) {
                          Navigator.of(context).pop(null);
                        }).catchError((err) =>
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text('Hubo un error' + err.toString()),
                              duration: Duration(seconds: 3),
                            )));
                      }
                    }))
          ]);
        }));
  }
}

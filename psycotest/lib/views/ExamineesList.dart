import 'package:flutter/material.dart';
import 'package:psycotest/models/Examinee.dart';
import 'package:psycotest/models/UserModel.dart';
import 'package:psycotest/resources/AlertMaker.dart';
import 'package:scoped_model/scoped_model.dart';
import '../views/ApplyProtocol/ApplyProtocolScreen.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:connectivity/connectivity.dart';

class ExamineesList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new ExamineesListState();
}

class ExamineesListState extends State<ExamineesList> {
  var _result = false;
  var _subscription;

  @override
  void initState() {
    super.initState();
    (Connectivity().checkConnectivity()).then((connectivityResult) {
      _result = (connectivityResult == ConnectivityResult.wifi ||
          connectivityResult == ConnectivityResult.mobile);
      this.setState(() => {_result});
    });
    _subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      _result = (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile);
      this.setState(() => {_result});
      if (!_result) {
        AlertMaker.showAlertDialog(
            context, "Parece que no tienes una conexión activa a internet!");
      }
    });
  }

  // Be sure to cancel subscription after you are done
  @override
  dispose() {
    super.dispose();
    _subscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    if (_result) {
      return FirebaseAnimatedList(
          query: FirebaseDatabase.instance.reference().child('pacientes'),
          defaultChild: new Center(
            child: CircularProgressIndicator(),
          ),
          itemBuilder: (context, snapshot, animation, index) {
            return ScopedModelDescendant<UserModel>(
                builder: (context, child, model) {
              var examinee = new Examinee.fromSnapshot(snapshot);
              if (model.examinees.firstWhere(
                      (current) => current.key == examinee.key,
                      orElse: () => null) ==
                  null) {
                model.examinees.add(examinee);
              }

              return new ExamineeItemCard(examinee, model);
            });
          });
    } else {
      return ScopedModelDescendant<UserModel>(builder: (context, child, model) {
        return new ListView.builder(
          itemCount: model.examinees.length,
          itemBuilder: (BuildContext context, int index) {
            return new ExamineeItemCard(model.examinees[index], model);
          },
        );
      });
    }
  }
}

class ExamineeItemCard extends StatelessWidget {
  final Examinee examinee;
  final UserModel model;

  ExamineeItemCard(this.examinee, this.model);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        margin: EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new ListTile(
              leading: Icon(
                Icons.people,
                size: 50.0,
              ),
              title: Container(
                padding: EdgeInsets.only(bottom: 16.0),
                child: Text(
                  examinee.name,
                  style: TextStyle(fontSize: 24.0),
                ),
              ),
              subtitle: Text(
                'Examinado ${examinee.name} nacido en la fecha ${examinee.birthdate} con identificación ${examinee.id}.',
                style: TextStyle(fontSize: 16.0),
              ),
            ),
            ButtonTheme.bar(
              child: ButtonBar(
                children: <Widget>[
                  FlatButton(
                    child: const Text('APLICAR PROTOCOLO'),
                    onPressed: () {
                      model.currentExaminee = examinee;
                      model.currentProtocol = null;
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ApplyProtocolScreen()),
                      );
                    },
                  ),
                  FlatButton(
                    child: const Text('EDITAR'),
                    onPressed: () {
                      /* ... */
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:psycotest/models/Submission.dart';
import 'package:psycotest/resources/StringFormat.dart';
import 'package:psycotest/views/TestApplication/TestApplicationController.dart';
import 'package:psycotest/views/widgets/FloatingActionButtonApplication.dart';
import 'package:sprintf/sprintf.dart';

import 'package:psycotest/models/UserModel.dart';
import 'package:scoped_model/scoped_model.dart';

import './ApplyProtocolForm.dart';
import '../../resources/strings.dart';

class ApplyProtocolScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<UserModel>(builder: (context, child, model) {
      return Scaffold(
        appBar: AppBar(
          title: Text(sprintf(Strings.APPLY_SOMETHING, [Strings.PROTOCOL])),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(null),
          ),
        ),
        body: Padding(
          padding: EdgeInsets.all(16.0),
          child: ApplyProtocolForm(),
        ),
        floatingActionButton: new FloatingActionButtonApplication(
          active:
              model.currentExaminee != null && model.currentProtocol != null,
          onPressed: () {
            model.currentSubmission = new Submission(
                DateTime.now(),
                model.currentTestsSelected);
            print("submission: " +
                model.currentSubmission.tests.first.toJson().toString());
            final databaseReference = FirebaseDatabase.instance.reference();
            var aRef = databaseReference
                .child('results')                
                .push();

                aRef.set({
                  "fecha": StringFormat.formatDate(model.currentSubmission.date.toString()),
                  "id":aRef.key,
                  "idPaciente":model.currentExaminee.key,
                  "idProtocolo": model.currentProtocol.id,
                  });
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => TestApplicationController()),
            );
          },
        ),
      );
    });
  }
}

import 'package:flutter/material.dart';
import 'package:psycotest/models/UserModel.dart';
import 'package:psycotest/views/widgets/AddTestsList.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../models/Examinee.dart';
import '../../models/Protocol.dart';
import '../../models/TestEditor/TestStructure.dart';

class ApplyProtocolForm extends StatefulWidget {
  @override
  ApplyProtocolFormState createState() {
    return ApplyProtocolFormState();
  }
}

class ApplyProtocolFormState extends State<ApplyProtocolForm> {
  final _formKey = GlobalKey<FormState>();
  double valSlider = 0.0;
  List<TestStructure> testToApply = new List();

  Widget _buildExamineeSection(Examinee examinee, UserModel model) {
    if (examinee == null) {
      return new FlatButton.icon(
          padding: EdgeInsets.all(12.0),
          icon: Icon(
            Icons.add,
            color: Colors.white,
          ),
          label: Text(
            "Agregar examinado",
            style: TextStyle(color: Colors.white),
          ),
          color: Colors.blueAccent,
          onPressed: () async => {
                model.currentExaminee = await Navigator.push(context,
                    MaterialPageRoute<Examinee>(
                        builder: (BuildContext context) {
                  return Scaffold(
                    body: ListView.builder(
                        itemCount: model.examinees.length,
                        itemBuilder: (BuildContext context, int index) {
                          return new GestureDetector(
                            child: Center(
                                child: Card(
                                    child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                  new ListTile(
                                    leading: Icon(Icons.people),
                                    title: Text(model.examinees[index].name),
                                  ),
                                ]))),
                            onTap: () {
                              Navigator.pop(context, model.examinees[index]);
                            },
                          );
                        }),
                  );
                }))
              });
    } else {
      return new Padding(
        child: Text(
          model.currentExaminee.name,
          style: TextStyle(fontSize: 14.0),
        ),
        padding: EdgeInsets.all(12.0),
      );
    }
  }

  Widget _buildProtocolSection(Protocol protocol, UserModel model) {
    if (protocol == null) {
      return new FlatButton.icon(
          padding: EdgeInsets.all(12.0),
          icon: Icon(
            Icons.add,
            color: Colors.white,
          ),
          label: Text(
            "Agregar protocolo",
            style: TextStyle(color: Colors.white),
          ),
          color: Colors.blue,
          onPressed: () async => {
                model.currentProtocol = await Navigator.push(context,
                    MaterialPageRoute<Protocol>(
                        builder: (BuildContext context) {
                  return Scaffold(
                    body: ListView.builder(
                        itemCount: model.protocols.length,
                        itemBuilder: (BuildContext context, int index) {
                          return new GestureDetector(
                            child: Center(
                                child: Card(
                                    child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                  new ListTile(
                                    leading: Icon(Icons.people),
                                    title: Text(model.protocols[index].name),
                                  ),
                                ]))),
                            onTap: () {
                              Navigator.pop(context, model.protocols[index]);
                            },
                          );
                        }),
                  );
                }))
              });
    } else {
      if (protocol.tests != null) {
        for (var test in protocol.tests) {
          TestStructure toAdd = model.tests.firstWhere((testInModel) {
            return test.id == testInModel.id;
          }, orElse: () => null);
          if (toAdd != null) {
            protocol.tests.remove(test);
            protocol.tests.add(toAdd);
          }
        }      
        model.currentProtocol = protocol;
        
      }
      return new Padding(
        child: Text(
          model.currentProtocol.name,
          style: TextStyle(fontSize: 14.0),
        ),
        padding: EdgeInsets.all(12.0),
      );
    }
  }

  void _changeElementInList(TestStructure test, bool add) {
    if (add) {
      testToApply.add(test);
    } else {
      testToApply.remove(test);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child:
            ScopedModelDescendant<UserModel>(builder: (context, child, model) {
          return ListView(children: <Widget>[
            Padding(
              child: Text(
                "Protocolo",
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              ),
              padding: EdgeInsets.all(12.0),
            ),
            _buildProtocolSection(model.currentProtocol, model),
            Padding(
              child: Text(
                "Examinado",
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              ),
              padding: EdgeInsets.all(12.0),
            ),
            _buildExamineeSection(model.currentExaminee, model),
            Padding(
              child: Text(
                "Tests",
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              ),
              padding: EdgeInsets.all(12.0),
            ),
            new AddTestList(
              elements: model.currentProtocol?.tests,
              changeElement: (TestStructure test, bool add) {
                if (add) {
                  testToApply.add(test);
                } else {
                  testToApply.remove(test);
                }
                model.currentTestsSelected = testToApply;
              },
            ),
            Padding(
              child: Center(
                child: Text(
                  "Progreso: " + valSlider.toString(),
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ),
              padding: EdgeInsets.all(12.0),
            ),
            Slider(
              value: 0.0,
              onChanged: null,
              min: 0.0,
              max: 100.0,
              label: "50.0",
            )
          ]);
        }));
  }
}

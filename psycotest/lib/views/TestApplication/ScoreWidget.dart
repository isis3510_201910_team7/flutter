import 'package:flutter/material.dart';
import '../../models/TestEditor/ScoreStructure.dart';

class ScoreWidget extends StatelessWidget {
  final NumericalScore numerical;
  final TextualScore textual;
  final onSave;
  ScoreWidget(this.onSave, {this.numerical, this.textual});

  Widget _buildNumericalScore() {
    int numberItems =
        (((numerical.end - numerical.initial) / numerical.jump)).round();
    numberItems++;
    return SizedBox(
        height: 50.0,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: numberItems,
          itemBuilder: (BuildContext context, int index) {
            int value = numerical.initial + (numerical.jump * index);
            return Container (
              margin: EdgeInsets.symmetric(horizontal:8.0),
              child:MaterialButton(
              minWidth: 50.0,
              color: Colors.deepOrange,
              child: Text(
                value.toString(),
                style: TextStyle(
                  fontSize: 20.0,
                ),
              ),
              onPressed: () {
                onSave(value);
              },
            )
            );
          },
        ));
  }

  Widget _buildTextualScore() {
    String dropdownValue = textual.options[0].text;
    List<String> values = new List();
    textual.options.map((option) {
      values.add(option.text);
    });
    return DropdownButton<String>(
      value: dropdownValue,
      onChanged: (value) {
        onSave(value);
      },
      items: values.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (numerical != null) {
      return _buildNumericalScore();
    } else if (textual != null) {
      return _buildTextualScore();
    } else {
      return Text("Element passed as paremeter is null. Cannot render");
    }
  }
}

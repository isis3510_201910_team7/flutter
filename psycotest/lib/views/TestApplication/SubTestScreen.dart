import 'package:flutter/material.dart';
import 'package:psycotest/models/TestEditor/SubTestStructure.dart';
class SubTestScreen extends StatelessWidget {

  final SubTestStructure subtest;

  const SubTestScreen(this.subtest);


  @override
  Widget build(BuildContext context) {
    print("Subtest screen : " + subtest.toJson().toString());
    return Center(
      child: Column(
        children: <Widget>[
          Text("Name: " + subtest.name),
          Text("Descripición: " + subtest.description),
          Text("Instrucciones: " + subtest.instructions),
        ],
      ),
    );
  }
  
}
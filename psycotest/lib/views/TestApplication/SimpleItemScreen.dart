import 'package:flutter/material.dart';
import '../../models/TestEditor/SimpleItemStructure.dart';

class SimpleItemScreen extends StatelessWidget {
  final SimpleItemStructure item;

  SimpleItemScreen(this.item);

  @override
  Widget build(BuildContext context) {
    return Text(item.name);
  }
}
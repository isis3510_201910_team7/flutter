import 'package:flutter/material.dart';
import 'package:psycotest/models/TestEditor/MessageStructure.dart';
import 'package:psycotest/models/TestEditor/SimpleItemStructure.dart';
import 'package:psycotest/models/TestEditor/SubTestStructure.dart';
import 'package:psycotest/models/TestEditor/SuperClasses/TestElement.dart';
import 'package:psycotest/models/TestEditor/TestStructure.dart';
import 'package:psycotest/models/UserModel.dart';
import 'package:psycotest/resources/strings.dart';
import 'package:psycotest/views/TestApplication/MessageWidget.dart';
import 'package:psycotest/views/TestApplication/SimpleItemScreen.dart';
import 'package:psycotest/views/TestApplication/SubTestScreen.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:sprintf/sprintf.dart';

class TestScreen extends StatefulWidget {
  final TestStructure currentTest;

  const TestScreen({this.currentTest});

  @override
  _TestScreenState createState() => _TestScreenState(currentTest: currentTest);
}

class _TestScreenState extends State<TestScreen> {
  TestStructure currentTest;

  _TestScreenState({this.currentTest});

  List<SubTestStructure> subtests = new List();
  List<SimpleItemStructure> items = new List();
  MessageStructure message;
  @override
  void initState() {
    super.initState();
    currentTest.elements.forEach((element) {
      if (element.getType() == ElementType.message) {
        message = element;
      } else if (element.getType() == ElementType.subtest) {
        subtests.add(element);
      } else if (element.getType() == ElementType.simpleItem) {
        items.add(element);
      }
    });
  }


  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(children: <Widget>[
        Padding(
          child: Text(
            "Subtests",
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
          ),
          padding: EdgeInsets.all(12.0),
        ),
        Container(
          height: 100.0,
          child: ListView.builder(
            itemCount: subtests.length,
            itemBuilder: (BuildContext context, int index) {
              return SubTestScreen(subtests[index]);
            },
          ),
        ),
        Padding(
          child: Text(
            "Items",
            style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
          ),
          padding: EdgeInsets.all(12.0),
        ),
        Container(
          height: 100.0,
          child: ListView.builder(
            itemCount: items.length,
            itemBuilder: (BuildContext context, int index) {
              return SimpleItemScreen(items[index]);
            },
          ),
        ),
      ]),
    );
  }
}

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:psycotest/models/Submission.dart';
import 'package:psycotest/models/TestEditor/MessageStructure.dart';
import 'package:psycotest/models/TestEditor/SimpleItemStructure.dart';
import 'package:psycotest/models/TestEditor/SubTestStructure.dart';
import 'package:psycotest/models/TestEditor/SuperClasses/TestElement.dart';
import 'package:psycotest/models/TestEditor/TestStructure.dart';
import 'package:psycotest/models/UserModel.dart';
import 'package:psycotest/resources/strings.dart';
import 'package:psycotest/views/ApplyProtocol/ApplyProtocolForm.dart';
import 'package:psycotest/views/TestApplication/MessageWidget.dart';
import 'package:psycotest/views/TestApplication/SimpleItemScreen.dart';
import 'package:psycotest/views/TestApplication/TestScreen.dart';
import 'package:psycotest/views/widgets/FloatingActionButtonApplication.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:sprintf/sprintf.dart';

class TestApplicationController extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TestApplicationControllerState();
  }
}

class TestApplicationControllerState extends State<TestApplicationController> {
  bool active = false;
  int index = 0;
  Submission current;

  @override
  void initState() {
    super.initState();
    
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<UserModel>(builder: (context, child, model) {
      current = model.currentSubmission;
      return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(sprintf(Strings.APPLY_SOMETHING, [Strings.PROTOCOL])),
        ),
        body: Padding(
          padding: EdgeInsets.all(16.0),
          child: _renderChild(model),
        ),
        floatingActionButton: new FloatingActionButtonApplication(
          active: this.active,
          onPressed: () {
            this.setState((){
              index++;
              active = false;
            });
            print("Now on item " + index.toString());
          },
        ),
      );
    });
  }

  Widget _renderChild(UserModel model) {
    if (current == null || current.tests == null || current.tests.length == 0) {
      return Center(
        child: Text("No hay items en estes protocolo para aplicar"),
      );
    } else if(index>= current.tests.length) {
      return Center(
        child: Column(
          children: <Widget>[
            Text("No hay items en estes protocolo para aplicar"),
            FlatButton(
              child: Text("Presiona para volver a home"),
              onPressed: () {
                model.currentExaminee = null;
                model.currentProtocol = null;
                model.currentSubmission = null;
                model.currentTestsSelected = new List();
                Navigator.popUntil(context, ModalRoute.withName('/'));
              },
            )
          ],
        ),
      );
    } else {
      TestStructure currentTest = current.tests[index];
      return TestScreen(currentTest: currentTest,);
    }
  }
}

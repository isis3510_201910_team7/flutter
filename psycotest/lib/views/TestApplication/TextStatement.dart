import 'package:flutter/material.dart';
import '../../models/TestEditor/TextStatement.dart';

class TextStatementWidget extends StatelessWidget {

  final TextStatement statement;
  TextStatementWidget(this.statement);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20.0),
      margin: EdgeInsets.all(20.0),
      color: Colors.grey,
      child:Text(statement.text,
    style: TextStyle(fontSize: 20.0),)
    );
  }

}
import 'package:flutter/material.dart';
import '../../models/TestEditor/QualificationTool.dart';

class QualificationToolWidget extends StatelessWidget {
  final QualificationTool tool;
  QualificationToolWidget(this.tool);

  Widget _buildCameraType() {
    return Text("Camera");
  }

    Widget _buildVoiceType() {
    return Text("Voice");
  }

    Widget _buildTimeType() {
    return Text("Time");
  }

    Widget _buildTextType() {
    return Text("Text");
  }

  @override
  Widget build(BuildContext context) {
    switch (tool.type) {
      case TOOL_TYPE.PHOTO_SHOOT:
        return _buildCameraType();
        case TOOL_TYPE.TEXT_AREA:
        return _buildTextType();
        case TOOL_TYPE.TIME_RECORD:
        return _buildTimeType();
        case TOOL_TYPE.VOICE_RECORD:
        return _buildVoiceType();
        default:
        return Text("Metodo de calificación no disponible");
    }
  }
  
}
import 'package:flutter/material.dart';
import '../../models/TestEditor/MessageStructure.dart';

class MessageWidget extends StatelessWidget {
  final MessageStructure messageStructure;
  MessageWidget(this.messageStructure);
  String _getTitle(){
    switch(messageStructure.type){
      case MESSAGE_TYPE.ALERT:
        return "Atención";
      case MESSAGE_TYPE.INFORMATION:
        return "Información";
      case MESSAGE_TYPE.WARNING:
        return "Cuidado";
      default:
        return "Información";
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: new Text(_getTitle()),
      content: new Text(messageStructure.text),
      actions: <Widget>[
        // usually buttons at the bottom of the dialog
        new FlatButton(
          child: new Text("Entendido"),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }
}
